<?php

declare(strict_types=1);

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Process\Process;

class DoctrineTest extends TestCase
{
    public function testDoctrineSchema(): void
    {
        $process = Process::fromShellCommandline('./bin/console doctrine:schema:update --dump-sql');
        $process->run();
        $this->assertStringContainsString('Nothing to update', $process->getOutput());
    }

    public function testDoctrineMapping(): void
    {
        $process = Process::fromShellCommandline('./bin/console doctrine:schema:validate');
        $code = $process->run();

        $this->assertEquals(0, $code, $process->getOutput());
    }
}
