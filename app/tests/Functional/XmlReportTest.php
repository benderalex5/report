<?php

declare(strict_types=1);

namespace App\Tests;

use Exception;
use SimpleXMLElement;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class XmlReportTest extends WebTestCase
{
    private const REPORT_URI = '/api/report/';

    protected KernelBrowser $client;

    public function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
    }

    /**
     * @throws Exception
     */
    public function testDownloadFile()
    {
        $this->client->request('GET', self::REPORT_URI . 'xml');
        $this->assertResponseIsSuccessful();
        $xmlData = $this->client->getInternalResponse()->getContent();
        $xml = new SimpleXMLElement($xmlData);
        $json = json_encode($xml);
        $jsonData = json_decode($json, true);
        $this->structureAssert($jsonData);
    }

    public function testJsonReport(): void
    {
        $this->client->request('GET', self::REPORT_URI . 'json');
        $this->assertResponseIsSuccessful();
        $jsonData = json_decode($this->client->getInternalResponse()->getContent(), true);
        $this->structureAssert($jsonData);
    }

    private function structureAssert(array $data): void
    {
        $this->assertArrayHasKey('teams', $data);
        $firstElement = reset($data['teams']);
        $this->assertArrayHasKey('name', $firstElement);
        $this->assertArrayHasKey('accounts', $firstElement);
        $this->assertIsArray($firstElement['accounts']);
    }
}
