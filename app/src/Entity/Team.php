<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\Controller\JsonReportController;
use App\Controller\XmlReportController;
use App\Repository\TeamRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/report/json',
            formats: 'json',
            controller: JsonReportController::class,
            openapiContext: [
                'summary' => 'Save report file as json',
                'description' => 'Save report file as json',
                'responses' => [
                    '200' => [
                        'description' => 'Download file as - 18_09_2022-21_39_55.json, where 18_09_2022-21_39_55 is current date and time',
                        'schema' => [
                            'type' => 'file'
                        ]
                    ]
                ]
            ],
            paginationEnabled: false,
            name: 'JsonReport'
        ),
        new GetCollection(
            uriTemplate: '/report/xml',
            formats: 'xml',
            controller: XmlReportController::class,
            openapiContext: [
                'summary' => 'Save report file as xml',
                'description' => 'Save report file as xml',
                'responses' => [
                    '200' => [
                        'description' => 'Download file as - 18_09_2022-21_39_55.xml, where 18_09_2022-21_39_55 is current date and time',
                        'schema' => [
                            'type' => 'file'
                        ]
                    ]
                ]
            ],
            paginationEnabled: false,
            name: 'XmlReport',
        ),
    ]
)]
#[ORM\Entity(repositoryClass: TeamRepository::class)]
class Team
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['report'])]
    private string $name;

    #[Groups(['report'])]
    private int $size;

    #[Groups(['report'])]
    #[ORM\OneToMany(mappedBy: 'team', targetEntity: Account::class, orphanRemoval: true)]
    private Collection $accounts;

    public function __construct()
    {
        $this->accounts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Account>
     */
    public function getAccounts(): Collection
    {
        return $this->accounts;
    }

    public function getSize(): int
    {
        return count($this->accounts);
    }
}
