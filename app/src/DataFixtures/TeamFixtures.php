<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Team;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class TeamFixtures extends Fixture
{
    public function __construct(
        protected ParameterBagInterface $params,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $accounts = $this->params->get('accounts') ?? 50;

        for ($i = 1; $i < 200; ++$i) {
            $team = new Team();
            $team->setName($faker->firstName);
            $manager->persist($team);
        }

        $manager->flush();
    }
}
