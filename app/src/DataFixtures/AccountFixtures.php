<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\Team;
use App\Repository\TeamRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Faker\Factory;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AccountFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        protected TeamRepository $teamRepository,
        protected ParameterBagInterface $params,
    ) {
    }

    /**
     * @throws Exception
     */
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $teams = $this->teamRepository->findAll();
        $accounts = $this->params->get('accounts');

        for ($i = 0; $i <= $accounts; ++$i) {
            $account = new Account();
            $account->setName($faker->firstName . ' ' . $faker->lastName);
            if ($teams[array_rand($teams)] instanceof Team) {
                $account->setTeam($teams[array_rand($teams)]);
            }
            $manager->persist($account);
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            TeamFixtures::class,
        ];
    }
}
