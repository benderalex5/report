<?php

declare(strict_types=1);

namespace App\Service;

use Negotiation\Exception\InvalidArgument;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class ReportFormat
{
    private const GROUP = 'report';
    private const ROOT = 'teams';

    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function format(array $data, string $format = 'json'): string
    {
        $formatters = [
            'xml' => $this->serializer->serialize(
                [self::ROOT => $data],
                XmlEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => self::GROUP]
            ),
            'json' => $this->serializer->serialize(
                [self::ROOT => $data],
                JsonEncoder::FORMAT,
                [AbstractNormalizer::GROUPS => self::GROUP, JsonEncode::OPTIONS => JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT]
            )
        ];

        if (!array_key_exists($format, $formatters)) {
            throw new InvalidArgument('Format not supported');
        }

        return $formatters[$format];
    }
}
