<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class JsonReportController extends ReportController
{
    public const REPORT_FORMAT = 'json';
}
