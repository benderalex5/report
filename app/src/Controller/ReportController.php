<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\ReportFormat;
use Symfony\Component\HttpFoundation\Response;

class ReportController
{
    private const REPORT_FORMAT = 'json';

    private ReportFormat $reportFormat;

    public function __construct(ReportFormat $reportFormat)
    {
        $this->reportFormat = $reportFormat;
    }

    public function __invoke(array $data): Response
    {
        $report = $this->reportFormat->format($data, static::REPORT_FORMAT);
        $response = new Response($report);
        $fileName = date('d_m_Y-H_i_s') . '.' . static::REPORT_FORMAT;
        $response->headers->set('Content-Disposition', "attachment; filename=$fileName");

        return $response;
    }
}
