# Report API


## First run
stop all other dockers
```bash
docker stop $(docker ps -a -q)
sudo sudo apt install docker-compose make -y
```
then
```bash
git clone https://gitlab.com/benderalex5/report.git && cd report && make up && make install && make test
```

## Load fixtures
```bash
make php
php bin/console doctrine:fixtures:load
```

## Open project
- Xml Report [http://0.0.0.0/api/report/xml](http://0.0.0.0/api/report/xml)
- Json Report [http://0.0.0.0/api/report/json](http://0.0.0.0/api/report/json)
- SwaggerUI  [http://neo.dev.local/api](http://local.formuladev.com/api/docs)

### What's inside

* [Nginx](http://nginx.org/)
* [PHP-FPM](http://php-fpm.org/)

### Requirements

* [Docker Engine](https://docs.docker.com/engine/install/)
* [Docker Compose](https://docs.docker.com/compose/install/)

### Makefile
This file helps to quickly interact with the work of docker and additional features.
Read about available commands `make help`

### Useful links

- [Api platform](https://api-platform.com/)
- [Symfony documentation](https://symfony.com/doc/current/index.html)

