#!make

default: build up

UID := $(shell id -u)

help:
	@echo ""
	@echo "usage: make COMMAND"
	@echo ""
	@echo "Commands:"
	@echo "  help                     List of all commands in make file"
	@echo "  build                    Build images"
	@echo "  install                  Install compose dependencies and run setup commands"
	@echo "  up                       Run docker compose"
	@echo "  php                      Bash to php container"
	@echo "  test                     Run tests"
	@echo "  stop                     Stop docker compose"
	@echo "  down                     Stop and removes containers, networks, volumes & images"
	@echo "  reinstall                Reinitialize project from scratch"
	@echo "  import                   Import data from API"

build:
	docker-compose build

up:
	docker-compose up -d

install:
	docker-compose exec -u$(UID) php composer install
	docker-compose exec -u$(UID) php ./bin/console doctrine:database:create --no-interaction
	docker-compose exec -u$(UID) php ./bin/console doc:mig:mig --no-interaction
	docker-compose exec -u$(UID) php bin/console --no-interaction doctrine:fixtures:load

test:
	docker-compose exec -u$(UID) php composer run test

stop:
	docker-compose stop

down:
	docker-compose down --volumes --rmi local --remove-orphans || true

php:
	docker-compose exec -u$(UID) php bash

reinstall: down build up install
